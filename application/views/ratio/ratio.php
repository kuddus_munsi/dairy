

<div class="content">
<div class="printarea">
    <div class="d-none d-print-block col-10 text-center mx-auto">
        <h4 class="font-weight-bold"> Appteum Agro</h4>
        <p>Salimpur, Chittagong.</p>
    </div>
    
    <div class="row">
        <div class="col-md-8">
            <form action="" id="addRatio" class="form-horizontal" method="POST">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Update Ratio</h4>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <label class="col-sm-2 col-form-label">Weight to Hay</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"> in Percentage(s): </span>
                                    </div>
                                    <input type="number" class="form-control test" name="weight_hay_feed_amount"
                                           value="0.5" step="0.01" min="0" max="100"/>

                                </div>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                <code>*</code>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                0.5 %
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 col-form-label">Weight to Green Fodder</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"> in Percentage(s): </span>
                                    </div>
                                    <input type="number" class="form-control test" name="weight_green_fodder_feed_amount"
                                           value="0.7" step="0.01" min="0" max="100"/>

                                </div>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                <code>*</code>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                0.7 %
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 col-form-label">Weight to Concentrate</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"> in Percentage(s): </span>
                                    </div>
                                    <input type="number" class="form-control test" name="weight_concentrate_feed_amount"
                                           value="1.5" step="0.01" min="0" max="100"/>


                                </div>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                <code>*</code>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                1.5 %
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 col-form-label">Lactation to Concentrate</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"> in Percentage(s): </span>
                                    </div>
                                    <input type="number" class="form-control test" name="lactation_concentrate_feed_amount"
                                           value="2" step="0.01" min="0" max="100"/>


                                </div>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                <code>*</code>
                            </div>
                            <div class="col-sm-1 label-on-right">
                                2 %
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2">

                            </div>
                            <div class="col-sm-8">
                                <button class="btn btn-default" type="submit">Save Ratio</button>
                            </div>

                        </div>

                    </div> <!-- card-body end -->
                </div>
            </form>
        </div>
    </div>


</div>
</div>
